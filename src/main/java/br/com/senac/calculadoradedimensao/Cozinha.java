/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.calculadoradedimensao;

/**
 *
 * @author Administrador
 */
public class Cozinha {

    private double comprimento;
    private double largura;
    private double altura;
    private double azulejo ;

    public Cozinha() {
    }

    public Cozinha(double comprimento, double largura, double altura, double azulejo) {
        this.comprimento = comprimento;
        this.largura = largura;
        this.altura = altura;
        this.azulejo = azulejo;
    }

    public Cozinha(int i, int i0, int i1) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public double getComprimento() {
        return comprimento;
    }

    public void setComprimento(double comprimento) {
        this.comprimento = comprimento;
    }

    public double getLargura() {
        return largura;
    }

    public void setLargura(double largura) {
        this.largura = largura;
    }

    public double getAltura() {
        return altura;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }

    public double getAzulejo() {
        return azulejo;
    }

    public void setAzulejo(double azulejo) {
        this.azulejo = azulejo;
    }
    
     public double getArea(){
        return this.comprimento * this.largura * this.altura ;
    }
    
    

}
